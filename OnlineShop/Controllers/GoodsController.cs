﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using OnlineShop.DAL.Data;
using OnlineShop.DAL.Data.Models;
using OnlineShop.DAL.Services.Interfaces;
using OnlineShop.Models;

namespace OnlineShop.Controllers
{
    public class GoodsController : Controller
    {
        private readonly IGoodService _goodService;
        
        private readonly IWebHostEnvironment _webHostEnvironment;
        private readonly ICategoryService _categoryService;

        public GoodsController(IGoodService service, IWebHostEnvironment webHostEnvironment, ICategoryService categoryService)
        {
            _goodService = service;
            _webHostEnvironment = webHostEnvironment;
            _categoryService = categoryService;
        }

        // GET: Goods
        public async Task<IActionResult> Index()
        {
            var goods = await _goodService.GetAllAsync();
            return View(goods);
        }

        // GET: Goods/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if(id == null)
            {
                return RedirectToAction(nameof(Index));
            }

            var good = await _goodService.GetByIdAsync(id.Value);

            if (good == null)
            {
                return NotFound();
            }

            return View(good);
        }

        // GET: Goods/Create
        public async Task<IActionResult> Create()
        {
            //TODO write category service
            ViewData["CategoryId"] = new SelectList(await _categoryService.GetAllAsync(), "Id", "Title");
            //ViewData["CategoryId"] = new SelectList(_context.Set<Category>(), "Id", "Title");
            return View();
        }

        // POST: Goods/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(GoodViewModel model)
        {
            if (ModelState.IsValid)
            {
                string uniqueFileName = await UploadedFile(model);

                Good good = new Good
                {
                    Title = model.Title,
                    Price = model.Price,
                    Description = model.Description,
                    CategoryId = model.CategoryId,
                    Image = uniqueFileName
                };

                await _goodService.CreateAsync(good);
                return RedirectToAction(nameof(Index));
            }

            ViewData["CategoryId"] = new SelectList(await _categoryService.GetAllAsync(), "Id", "Title", model.CategoryId);
            //ViewData["CategoryId"] = new SelectList(_context.Set<Category>(), "Id", "Title", model.CategoryId);
            return View(model);
        }

        // GET: Goods/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if(id == null)
            {
                RedirectToAction(nameof(Index));
            }

            var good = await _goodService.GetByIdAsync(id.Value);
            
            if (good == null)
            {
                return NotFound();
            }

            var goodModel = new GoodViewModel
            {
                Id = good.Id,
                Title = good.Title,
                Description = good.Description,
                Category = good.Category,
                CategoryId = good.CategoryId,
                Price = good.Price,
                Image = await GetImage(good.Image)
            };

            TempData["file"] = good.Image;
            ViewData["CategoryId"] = new SelectList(await _categoryService.GetAllAsync(), "Id", "Title", good.CategoryId);
            //ViewData["CategoryId"] = new SelectList(_context.Set<Category>(), "Id", "Title", good.CategoryId);
            return View(goodModel);
        }

        // POST: Goods/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int? id, GoodViewModel model)
        {
            if(id == null)
            {
                RedirectToAction(nameof(Index));
            }

            if (id != model.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                string uniqueFileName = model.Image == null ? TempData["file"]?.ToString() : await UploadedFile(model);

                Good good = new Good
                {
                    Id = model.Id,
                    Title = model.Title,
                    Price = model.Price,
                    Description = model.Description,
                    CategoryId = model.CategoryId,
                    Image = uniqueFileName
                };

                await _goodService.UpdateAsync(good);
                return RedirectToAction(nameof(Index));
            }
            ViewData["CategoryId"] = new SelectList(await _categoryService.GetAllAsync(), "Id", "Title", model.CategoryId);
            //ViewData["CategoryId"] = new SelectList(_context.Set<Category>(), "Id", "Title", model.CategoryId);
            return View(model);
        }

        // GET: Goods/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if(id == null)
            {
                RedirectToAction(nameof(Index));
            }

            var good = await _goodService.GetByIdAsync(id.Value);
            
            if (good == null)
            {
                return NotFound();
            }

            return View(good);
        }

        // POST: Goods/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int? id)
        {
            if(id == null)
            {
                RedirectToAction(nameof(Index));
            }

            if (await _goodService.DeleteByIdAsync(id.Value))
            {
                return RedirectToAction(nameof(Index));
            }

            return BadRequest();
        }

        private async Task<string> UploadedFile(GoodViewModel model)
        {
            string uniqueFileName = null;

            if (model.Image != null)
            {
                string uploadsFolder = Path.Combine(_webHostEnvironment.WebRootPath, "images");
                uniqueFileName = Guid.NewGuid().ToString() + "_" + model.Image.FileName;
                string filePath = Path.Combine(uploadsFolder, uniqueFileName);
                using (var fileStream = new FileStream(filePath, FileMode.Create))
                {
                    await model.Image.CopyToAsync(fileStream);
                }
            }
            return uniqueFileName;
        }

        private async Task<IFormFile> GetImage(string path)
        {
            try
            {
                using var stream = new MemoryStream((await System.IO.File.ReadAllBytesAsync("wwwroot/images/" + path)).ToArray());
                var formFile = new FormFile(stream, 0, stream.Length, "streamFile", path.Split(@"\").Last());

                return formFile;
            }
            catch
            {
                return null;
            }
        }
    }
}
