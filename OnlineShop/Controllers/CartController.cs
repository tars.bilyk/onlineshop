﻿using Microsoft.AspNetCore.Mvc;
using OnlineShop.DAL.Data.Models;
using OnlineShop.DAL.Services.Interfaces;
using OnlineShop.Helpers;
using OnlineShop.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineShop.Controllers
{
    public class CartController : Controller
    {
        private IGoodService _service;
        public CartController(IGoodService service)
        {
            _service = service;
        }
        public IActionResult Index()
        {
            var cart = SessionHelper.GetObjectFromJson<List<OrderItemViewModel>>(HttpContext.Session, "cart");

            if(cart == null)
            {
                cart = new List<OrderItemViewModel>();
            }

            ViewBag.totalPrice = cart.Select(i => i.Quantity * i.Good.Price).Sum();
            return View(cart);
        }

        public async Task<IActionResult> Buy(int id)
        {
            var cart = SessionHelper.GetObjectFromJson<List<OrderItemViewModel>>(HttpContext.Session, "cart");
            if (cart == null)
            {
                cart = new List<OrderItemViewModel>();
            }
            var good = await _service.GetByIdAsync(id);
            if (good == null)
            {
                return NotFound();
            }
            cart.Add(new OrderItemViewModel { Good = good, Quantity = 1, Id = cart.Count() });
            SessionHelper.SetObjectAsJson(HttpContext.Session, "cart", cart);
            return RedirectToAction("Index");
        }

        public IActionResult Delete(int id)
        {
            var cart = SessionHelper.GetObjectFromJson<List<OrderItemViewModel>>(HttpContext.Session, "cart");

            var item = cart.FirstOrDefault(i => i.Id == id);


            if (item == null)
            {
                return NotFound();
            }

            cart.Remove(item);
            SessionHelper.SetObjectAsJson(HttpContext.Session, "cart", cart);
            return RedirectToAction("Index");
        }

        // GET: Cart/Edit/5
        public IActionResult Edit(int id)
        {
            var cart = SessionHelper.GetObjectFromJson<List<OrderItemViewModel>>(HttpContext.Session, "cart");
            var cartItem = cart.FirstOrDefault(i => i.Id == id);
            if (cartItem == null)
            {
                return NotFound();
            }

            return View(cartItem);
        }

        // POST: Cart/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(int id, OrderItemViewModel orderItem)
        {
            if (ModelState.IsValid)
            {
                var cart = SessionHelper.GetObjectFromJson<List<OrderItemViewModel>>(HttpContext.Session, "cart");
                var cartItem = cart.FirstOrDefault(i => i.Id == id);
                if(cartItem == null)
                {
                    return NotFound();
                }
                cartItem.Quantity = orderItem.Quantity;
                SessionHelper.SetObjectAsJson(HttpContext.Session, "cart", cart);
                return RedirectToAction("Index");
            }
            return View(orderItem);
        }
    }
}
