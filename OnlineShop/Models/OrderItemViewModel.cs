﻿using OnlineShop.DAL.Data.Models;

namespace OnlineShop.Models
{
    public class OrderItemViewModel
    {
        public int Id { get; set; }
        public Good Good { get; set; }
        public int Quantity { get; set; }
    }
}
