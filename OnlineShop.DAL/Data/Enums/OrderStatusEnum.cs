﻿namespace OnlineShop.DAL.Data.Enums
{
    public enum OrderStatusEnum
    {
        InProgress,
        Arrived,
        Closed
    }
}
