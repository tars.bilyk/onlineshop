﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace OnlineShop.DAL.Data.Models
{
    public class Good : BaseEntity
    {
        public string Title { get; set; }

        [Column(TypeName = "decimal(18,2)")]
        [Range(0, 1000000, ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        public decimal Price { get; set; }
        public string Description { get; set; }
        [JsonIgnore]
        [IgnoreDataMember]
        public virtual Category Category { get; set; }
        public int CategoryId { get; set; }
        public string Image { get; set; }
    }
}
