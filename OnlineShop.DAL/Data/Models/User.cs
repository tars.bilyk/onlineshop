﻿using Microsoft.AspNetCore.Identity;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace OnlineShop.DAL.Data.Models
{
    public class User : IdentityUser
    {
        [JsonIgnore]
        [IgnoreDataMember]
        public virtual ICollection<Order> Orders { get; set; }
    }
}
