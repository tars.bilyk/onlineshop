﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace OnlineShop.DAL.Data.Models
{
    public class Category : BaseEntity
    {
        public string Title { get; set; }
        [JsonIgnore]
        [IgnoreDataMember]
        public virtual ICollection<Good> Goods { get; set; }
    }
}
