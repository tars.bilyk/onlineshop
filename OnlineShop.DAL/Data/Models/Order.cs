﻿using Newtonsoft.Json;
using OnlineShop.DAL.Data.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;


namespace OnlineShop.DAL.Data.Models
{
    public class Order : BaseEntity
    {
        [JsonIgnore]
        [IgnoreDataMember]
        public virtual ICollection<OrderItem> OrderItems { get; set; }
        public string UserId { get; set; }
        [JsonIgnore]
        [IgnoreDataMember]
        public virtual User User { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal TotalPrice => OrderItems.Sum(o => o.TotalPrice);
        public DateTime Date { get; set; }
        public OrderStatusEnum OrderStatus { get; set; } 
    }
}
