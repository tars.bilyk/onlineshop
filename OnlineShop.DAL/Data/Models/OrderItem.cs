﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace OnlineShop.DAL.Data.Models
{
    public class OrderItem : BaseEntity
    {
        //[ForeignKey("Good")]
        public int GoodId { get; set; }
        [JsonIgnore]
        [IgnoreDataMember]
        public virtual Good Good { get; set; }
        public int OrderId { get; set; }
        [JsonIgnore]
        [IgnoreDataMember]
        public virtual Order Order { get; set; }
        public int Quantity { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal TotalPrice => Good.Price * Quantity;
    }
}
