﻿using Microsoft.EntityFrameworkCore;
using OnlineShop.DAL.Data.Models;
using System;
using OnlineShop.DAL.Data.Enums;

namespace OnlineShop.DAL.Data
{
    public static class ModelBuilderExtensions
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Category>().HasData(
                new Category { Id = 1, Title = "Category1" },
                new Category { Id = 2, Title = "Category2" },
                new Category { Id = 3, Title = "Category3" }
                );

            modelBuilder.Entity<Good>().HasData(
                new Good { Id = 1, Title = "Good1", Description = "Description1", Price = 10.25m, CategoryId = 1, Image = "image1.jpg" },
                new Good { Id = 2, Title = "Good2", Description = "Description2", Price = 15.50m, CategoryId = 2, Image = "image2.jpg" },
                new Good { Id = 3, Title = "Good3", Description = "Description3", Price = 12.10m, CategoryId = 3, Image = "image3.jpg" },
                new Good { Id = 4, Title = "Good4", Description = "Description4", Price = 14.55m, CategoryId = 3, Image = "image4.jpg" }
                );

            modelBuilder.Entity<OrderItem>().HasData(
                new OrderItem { Id = 1, GoodId = 1, Quantity = 5, OrderId = 1 },
                new OrderItem { Id = 2, GoodId = 2, Quantity = 2, OrderId = 1 },
                new OrderItem { Id = 3, GoodId = 3, Quantity = 3, OrderId = 2 },
                new OrderItem { Id = 4, GoodId = 3, Quantity = 1, OrderId = 3 }
                );

            //modelBuilder.Entity<User>().HasData(
            //    new User { Id = "1", Email = "1@email", UserName = "user1" },
            //    new User { Id = "2", Email = "2@email", UserName = "user2" },
            //    new User { Id = "3", Email = "3@email", UserName = "user3" },
            //    new User { Id = "4", Email = "4@email", UserName = "user4" }
            //    );

            modelBuilder.Entity<Order>().HasData(
                new Order { Id = 1, UserId = "1", Date = new DateTime(23 / 12 / 2021), OrderStatus = OrderStatusEnum.InProgress },
                new Order { Id = 2, UserId = "2", Date = new DateTime(12 / 12 / 2021), OrderStatus = OrderStatusEnum.InProgress },
                new Order { Id = 3, UserId = "3", Date = new DateTime(10 / 08 / 2021), OrderStatus = OrderStatusEnum.Closed }
                );
        }
    }
}
