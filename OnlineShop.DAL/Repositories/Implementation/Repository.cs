﻿using Microsoft.EntityFrameworkCore;
using OnlineShop.DAL.Data;
using OnlineShop.DAL.Data.Models;
using OnlineShop.DAL.Repositories.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OnlineShop.DAL.Repositories.Implementation
{
    public abstract class Repository<T> : IRepository<T> where T : BaseEntity
    {
        protected ApplicationDbContext DbContext { get; set; }
        protected DbSet<T> DbSet { get; set; }

        public Repository(ApplicationDbContext dbContext)
        {
            DbContext = dbContext;
            DbSet = DbContext.Set<T>();
        }
        public async Task CreateAsync(T item)
        {
            await DbSet.AddAsync(item);
        }

        public async Task DeleteAsync(T item)
        {
            await Task.Run(() => DbSet.Remove(item));
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            var a =  await DbSet.ToListAsync();
            return a;
        }

        public async Task<T> GetByIdAsync(int id)
        {
            return await DbSet.FindAsync(id);
        }

        public async Task UpdateAsync(T item)
        {
            await Task.Run(() => DbSet.Update(item));
        }
    }
}
