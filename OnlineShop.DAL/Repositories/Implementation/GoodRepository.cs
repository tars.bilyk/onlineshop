﻿using OnlineShop.DAL.Data;
using OnlineShop.DAL.Data.Models;
using OnlineShop.DAL.Repositories.Interfaces;

namespace OnlineShop.DAL.Repositories.Implementation
{
    public class GoodRepository : Repository<Good>, IGoodRepository
    {
        public GoodRepository(ApplicationDbContext dbContext) : base(dbContext)
        {

        }
    }
}
