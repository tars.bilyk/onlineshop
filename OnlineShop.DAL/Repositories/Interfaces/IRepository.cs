﻿using OnlineShop.DAL.Data.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OnlineShop.DAL.Repositories.Interfaces
{
    public interface IRepository<T> where T : BaseEntity
    {
        Task<IEnumerable<T>> GetAllAsync();
        Task<T> GetByIdAsync(int id);
        Task CreateAsync(T item);
        Task UpdateAsync(T item);
        Task DeleteAsync(T item);
    }
}
