﻿using OnlineShop.DAL.Data.Models;

namespace OnlineShop.DAL.Repositories.Interfaces
{
    public interface IGoodRepository : IRepository<Good>
    {
    }
}
