﻿using OnlineShop.DAL.Data.Models;

namespace OnlineShop.DAL.Services.Interfaces
{
    public interface ICategoryService : IService<Category>
    {

    }
}
