﻿using OnlineShop.DAL.Data.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OnlineShop.DAL.Services.Interfaces
{
    public interface IService<T> where T : BaseEntity
    {
        Task<IEnumerable<T>> GetAllAsync();
        Task<T> GetByIdAsync(int id);
        Task CreateAsync(T item);
        Task<bool> DeleteByIdAsync(int id);
        Task UpdateAsync(T item);

    }
}
