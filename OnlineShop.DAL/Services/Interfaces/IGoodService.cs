﻿using OnlineShop.DAL.Data.Models;

namespace OnlineShop.DAL.Services.Interfaces
{
    public interface IGoodService : IService<Good>
    {
    }
}
