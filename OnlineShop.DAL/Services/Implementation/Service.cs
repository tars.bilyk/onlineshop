﻿using OnlineShop.DAL.Data.Models;
using OnlineShop.DAL.Repositories.Interfaces;
using OnlineShop.DAL.Services.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OnlineShop.DAL.Services.Implementation
{
    public abstract class Service<T> : IService<T> where T : BaseEntity
    {
        protected IRepository<T> Repository { get; set; }
        protected IUnitOfWork UnitOfWork { get; set; }

        public Service(IRepository<T> repository, IUnitOfWork unitOfWork)
        {
            Repository = repository;
            UnitOfWork = unitOfWork;
        }
        public async Task CreateAsync(T item)
        {
            await Repository.CreateAsync(item);
            await UnitOfWork.CommitAsync();
        }

        public async Task<bool> DeleteByIdAsync(int id)
        {
            var item = await Repository.GetByIdAsync(id);
            if(item == null)
            {
                return false;
            }
            await Repository.DeleteAsync(item);
            await UnitOfWork.CommitAsync();
            return true;
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await Repository.GetAllAsync();
        }

        public async Task<T> GetByIdAsync(int id)
        {
            return await Repository.GetByIdAsync(id);
        }

        public async Task UpdateAsync(T item)
        {
            await Repository.UpdateAsync(item);
            await UnitOfWork.CommitAsync();
        }
    }
}
