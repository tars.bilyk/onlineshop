﻿using OnlineShop.DAL.Data.Models;
using OnlineShop.DAL.Repositories.Interfaces;
using OnlineShop.DAL.Services.Interfaces;

namespace OnlineShop.DAL.Services.Implementation
{
    public class CategoryService : Service<Category>, ICategoryService
    {
        public CategoryService(ICategoryRepository repository, IUnitOfWork unitOfWork) : base(repository, unitOfWork)
        {

        }
    }
}
