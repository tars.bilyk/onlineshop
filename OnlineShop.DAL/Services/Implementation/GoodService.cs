﻿using OnlineShop.DAL.Data.Models;
using OnlineShop.DAL.Repositories.Interfaces;
using OnlineShop.DAL.Services.Interfaces;

namespace OnlineShop.DAL.Services.Implementation
{
    public class GoodService : Service<Good>, IGoodService
    {
        public GoodService(IGoodRepository repository, IUnitOfWork unitOfWork) : base(repository, unitOfWork)
        {

        }
    }
}
