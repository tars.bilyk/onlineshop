﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace OnlineShop.DAL.Migrations
{
    public partial class AddedImageSeed : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "1",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "9171cc8c-e35f-438a-9747-ccb1be912f5e", "d5405da9-bf0e-431f-8e45-c9b892ea78c5" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "2",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "f06cd3d5-480c-48a2-a1e6-92172f0bf8df", "dc0b4daf-acb9-4855-a1f4-721e0f6a4b09" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "3",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "80ccb7b0-5ff6-4f50-89ef-3b5f6d643d1b", "ca4f96fb-3484-44c5-ad15-56b1dd099340" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "4",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "7a6f1e53-8e6b-4f02-bb9a-ca1243f8f8c7", "02da8b34-7ebd-42f9-86ca-0af40c033fa2" });

            migrationBuilder.UpdateData(
                table: "Goods",
                keyColumn: "Id",
                keyValue: 1,
                column: "Image",
                value: "image1.jpg");

            migrationBuilder.UpdateData(
                table: "Goods",
                keyColumn: "Id",
                keyValue: 2,
                column: "Image",
                value: "image2.jpg");

            migrationBuilder.UpdateData(
                table: "Goods",
                keyColumn: "Id",
                keyValue: 3,
                column: "Image",
                value: "image3.jpg");

            migrationBuilder.UpdateData(
                table: "Goods",
                keyColumn: "Id",
                keyValue: 4,
                column: "Image",
                value: "image4.jpg");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "1",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "fe581ef4-cd8d-458e-b6e2-daf32ead406f", "d88509ad-9162-4443-afb9-6bcdce5d2974" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "2",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "5385b4ed-6363-4738-bc14-4a0d96294042", "e1c4808b-a201-48f7-a307-864d85fe6017" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "3",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "b5c32ddb-98b5-4459-9f99-0aad82e596c2", "2f3e7018-ee42-4335-9f57-f8d5a5526c69" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "4",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "f3b007b9-3a81-455b-9b96-19d4eeef3c35", "13f424ef-e7d0-4e25-bb95-2da69edc107c" });

            migrationBuilder.UpdateData(
                table: "Goods",
                keyColumn: "Id",
                keyValue: 1,
                column: "Image",
                value: null);

            migrationBuilder.UpdateData(
                table: "Goods",
                keyColumn: "Id",
                keyValue: 2,
                column: "Image",
                value: null);

            migrationBuilder.UpdateData(
                table: "Goods",
                keyColumn: "Id",
                keyValue: 3,
                column: "Image",
                value: null);

            migrationBuilder.UpdateData(
                table: "Goods",
                keyColumn: "Id",
                keyValue: 4,
                column: "Image",
                value: null);
        }
    }
}
