﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace OnlineShop.DAL.Migrations
{
    public partial class Seedgoods : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Goods",
                columns: new[] { "Id", "Description", "Price", "Title" },
                values: new object[] { 1, "Description1", 10m, "Good1" });

            migrationBuilder.InsertData(
                table: "Goods",
                columns: new[] { "Id", "Description", "Price", "Title" },
                values: new object[] { 2, "Description2", 20m, "Good2" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Goods",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Goods",
                keyColumn: "Id",
                keyValue: 2);
        }
    }
}
