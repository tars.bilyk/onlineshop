﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace OnlineShop.DAL.Migrations
{
    public partial class AddedImageSrctoGood : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ImageSrc",
                table: "Goods",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "1",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "d0cb1620-9297-4ac0-9f2d-d9c2108206fc", "a2da87da-712c-4e51-af9c-c9bdbcca0cdc" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "2",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "eba61414-d398-4fa2-9124-08b8c8cbb234", "156818d8-be19-40dd-b5ab-67b0e37acda9" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "3",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "7b3affda-b9b0-464f-a0ce-ddaf7cd9d176", "d9a566b8-ead1-4598-81ac-f9f9609153fb" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "4",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "f5d1a967-36b9-4be9-8928-4f4e5b21a836", "49eefae5-7e31-4fc4-828d-6ee3c2224491" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ImageSrc",
                table: "Goods");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "1",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "80fc40d6-02eb-4464-846e-51aafc38bd09", "45a6a019-d98d-435a-9906-2f720af95757" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "2",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "17a89150-f182-49d5-8ecf-cacd4c6e1671", "871a92d5-d21b-49ab-ab42-7fc50bda5836" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "3",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "d54043c1-8454-435a-a160-b1134c1214bd", "5f636783-dcdb-4ba0-8586-8e64bd2555ae" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "4",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "dedd7a1f-a1ab-4db0-a9e3-2106332c2ddc", "700c1cc7-720f-4e75-87c3-f6fd6ec9cacf" });
        }
    }
}
