﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace OnlineShop.DAL.Migrations
{
    public partial class ChangedImageinGood : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ImageSrc",
                table: "Goods",
                newName: "Image");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "1",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "70d02b12-3d98-4221-9d6e-7a4ee6309e5d", "df034a59-9c74-4b4f-8ece-64607b1a14ad" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "2",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "3009b3ce-ff1c-46c5-8ebd-869b7ab1435d", "6334632c-08c1-463b-abcf-8fc99f9c5e65" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "3",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "2ea47d2e-f596-405a-8ae4-e7a4e1e41600", "cb9fab8c-0f10-4c26-94e9-ad40513d1278" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "4",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "cbd042c3-850f-49bb-99b2-af8c1f721232", "e52fff08-f098-44fb-8d71-54aa725b032e" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Image",
                table: "Goods",
                newName: "ImageSrc");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "1",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "d0cb1620-9297-4ac0-9f2d-d9c2108206fc", "a2da87da-712c-4e51-af9c-c9bdbcca0cdc" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "2",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "eba61414-d398-4fa2-9124-08b8c8cbb234", "156818d8-be19-40dd-b5ab-67b0e37acda9" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "3",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "7b3affda-b9b0-464f-a0ce-ddaf7cd9d176", "d9a566b8-ead1-4598-81ac-f9f9609153fb" });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "4",
                columns: new[] { "ConcurrencyStamp", "SecurityStamp" },
                values: new object[] { "f5d1a967-36b9-4be9-8928-4f4e5b21a836", "49eefae5-7e31-4fc4-828d-6ee3c2224491" });
        }
    }
}
